#include "View.h"
#include <iostream>
#include <conio.h>
#include "Arena.h"
#include "CharacterSelection.h"
using namespace std;
View::View() = default;
View::~View() = default;

View& View::getInstance() {
	static View v;
	return v;
}


void View::open(size_t w, size_t h, std::string title, Model& model, GameSM & gsm)
{
	width = w;
	height = h;
	mWindow.create(sf::VideoMode(width, height), title);
	playerList = model.getPlayerList();
	gamesm = gsm;
}

void View::gameLoop() {
	if (!font.loadFromFile("../font/ARCADECLASSIC.TTF")) {
		throw "doesnt work";
	}

	
	while (mWindow.isOpen()) {
		sf::Event event;
		
		while (mWindow.pollEvent(event))
		{
			/*Window is closed by pressing X of the window*/
			if (event.type == sf::Event::Closed) {
				mWindow.close();
			}

			findEvent(event);
		}

		drawStates();
		mWindow.clear();


	}
	
}

void View::findEvent(sf::Event event) {
	MainMenu * testMain{ dynamic_cast<MainMenu*>(mainmenu) };


	if (event.type == sf::Event::JoystickConnected) {
		playerList.push_back(new Player(event.joystickConnect.joystickId));
	}

	else if (event.type == sf::Event::JoystickDisconnected){
		/*Make sure the pointer is not dangeling before removing it from the vector*/
		playerList.at(event.joystickConnect.joystickId) = nullptr;
		delete playerList.at(event.joystickConnect.joystickId);
		playerList.erase(playerList.begin() + event.joystickConnect.joystickId);
	}

	/*Button is pressed on GamePad info saved index of button and gamepad*/
	else if (event.type == sf::Event::JoystickButtonPressed) {
		/*std::cout << event.joystickButton.button;*/

			if (playerList.size() <= event.joystickButton.joystickId) {
				playerList.push_back(new Player(event.joystickButton.joystickId));
			}
			playerList.at(event.joystickButton.joystickId)->isPressed(event.joystickButton.button);

	}

	else if (event.type == sf::Event::JoystickButtonReleased) {
		/*std::cout << event.joystickButton.button;*/
		if (playerList.size() <= event.joystickButton.joystickId) {
			playerList.push_back(new Player(event.joystickButton.joystickId));
		}
		playerList.at(event.joystickButton.joystickId)->isPressed(-1);
	}

	/*Axis is moved on GamePad info saved index of axis and gamepad and position of axis*/
	else if (event.type == sf::Event::JoystickMoved) {
		if (playerList.size() <= event.joystickMove.joystickId) {
			playerList.push_back(new Player(event.joystickMove.joystickId));
		}
		playerList.at(event.joystickMove.joystickId)->isMoved(event.joystickMove.axis, event.joystickMove.position);
	}
}

void View::drawMainMenu()
{
	if (gamesm.current.getStateName() != lastState.getStateName()) {
		mainmenu = new MainMenu(width, height, font, playerList, mWindow);
		//Don't forget to type cast to able to use the methods of the children of Surface
		lastState = gamesm.current;
	}
	MainMenu * testMain{ dynamic_cast<MainMenu*>(mainmenu) };
	testMain->action(gamesm);
	mainmenu->draw();
	mWindow.display();

}

void View::drawCharacterSelect() {
	if (gamesm.current.getStateName() != lastState.getStateName()) {
		mainmenu = new CharacterSelection(width, height, font, playerList, mWindow);
		lastState = gamesm.current;
	}

	mainmenu->draw();
	mWindow.display();
}

void View::drawInGame()
{
	if (gamesm.current.getStateName() != lastState.getStateName()) {
		mainmenu = new Arena(width, height, font, playerList, mWindow);
		lastState = gamesm.current;
	}
		//Don't forget to type cast to able to use the methods of the children of Surface
		Arena * testMain{ dynamic_cast<Arena*>(mainmenu) };
		testMain->update();
		mainmenu->draw();
		mWindow.display();
}

void View::drawStates()
{
	if (gamesm.current.getStateName() == "mainMenu") {
		drawMainMenu();
	}
	else if (gamesm.current.getStateName() == "quit") {
		exit(0);
	}

	else if (gamesm.current.getStateName() == "nameSelect") {
		drawCharacterSelect();
	}
}
