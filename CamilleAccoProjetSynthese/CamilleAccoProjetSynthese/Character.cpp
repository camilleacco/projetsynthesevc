#include "Character.h"

Character::Character(std::string image, double speed, size_t columns, size_t rows, size_t size, size_t strenght, size_t defense)
{
	setImg(image);
	setSpeed(speed);
	setColumns(columns);
	setRows(rows);
	setSize(size);
	setStrenght(strenght);
	anim = new Animation(image, columns, rows, speed, size);
	

}



void Character::setImg(std::string i)
{
	image = i;
}

std::string Character::getImg()
{
	return image;
}

void Character::setSize(size_t size)
{
	this->size = size;
}

size_t Character::getSize()
{
	return size;
}

void Character::setColumns(size_t col)
{
	columns = col;
}

size_t Character::getColumns()
{
	return columns;
}

void Character::setRows(size_t rows)
{
	this->rows = rows;
}

size_t Character::getRows()
{
	return rows;
}

Animation* Character::getAnim() {
	return anim;
}

void Character::setSpeed(double sp)
{
	speed = sp;
}

double Character::getSpeed()
{
	return speed;
}

void Character::setStrenght(size_t st)
{
	strenght = st;
}

size_t Character::getStrenght()
{
	return strenght;
}

void Character::setDefense(size_t d)
{
	defense = d;
}

size_t Character::getDefense()
{
	return defense;
}





