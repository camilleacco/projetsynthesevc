#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include<map>

#include "StateManager.h"
#include "Character.h"
#include "CharacterSM.h"
#include "HurtBox.h"
#include "HitBox.h"


class Player
{
public:
	Player(size_t index);
	void setUpArena();
	void setnewChunLi();
	void setnewDhalsim();
	void setnewKen();
	void setnewMBison();
	void setnewRyu();
	void setnewZanglief();
	~Player() = default;

	size_t getIndex();
	void setLife(size_t life);
	size_t getLife();
	void isPressed(int move);
	void isMoved(size_t axis, float position);
	int getLastButton();
	int getLastMove();
	void setPoints(size_t points);
	size_t getPoints();
	void setRagePoints(size_t rageMeter);
	size_t getRagePoints();
	bool isAlive();
	bool isInRage();
	void attack();
	void defend();	void animSetValues();
	Animation* getAnimation();
	CharacterSM& getCharacterSM();
	sf::Clock& getClock();
	void initialPosition();
	HurtBox* getHurtBox();
	HitBox* getHitBox();

private: 
	size_t index, life{ 100 }, points{ 0 }, rageMeter{ 0 };
	const int deadZone{ 50 };
	int lastMove, lastButton { -1 };
	bool alive{ true }, inRage{ false };
	std::map <std::string, std::tuple <int, int> > spriteData;  //value from StateManager is key (only for the values that have to do with the character)
																// tuple is row and max column
	Character* charac;
	CharacterSM csm;
	HurtBox* hurtBox;
	HitBox* hitBox;
	sf::Clock playerClock;
	void translateAxis(size_t axis, float position);
	void fillMap();

};
#endif // !PLAYER_H
