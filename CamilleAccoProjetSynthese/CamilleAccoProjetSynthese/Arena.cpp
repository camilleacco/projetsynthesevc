#include "Arena.h"
#include "ButtonMap.h"
#include <iostream>


Arena::Arena(size_t const & width, size_t const & height, sf::Font const & font, std::vector<Player*>& listPlayer, sf::RenderWindow& window) : Surface(width, height, font, listPlayer, window)
{
	fill();
}

void Arena::fill()
{
	for (auto p : playerList) {
		// Putting back into default after x was pressed
		p->isPressed(-1);
		p->setUpArena();
	}

}

void Arena::update()
{
	for (auto p : playerList) {
		p->getHurtBox()->setNormal(p->getAnimation()->getSprite());
		if (p->getCharacterSM().current.getStateName() == "idle") {
			isIdle(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "walk") {
			if (p->getAnimation()->getFlipped()) {
				if (p->getAnimation()->getPosX() - 1 > 0) {
					p->getAnimation()->setPosX(p->getAnimation()->getPosX() -1);
				}
			}

			else {
				if (p->getAnimation()->getPosX() + 1 < mWindow.getSize().x) {
					p->getAnimation()->setPosX(p->getAnimation()->getPosX() + 1);
					
				}
			}
			p->getCharacterSM().process(StateManager::c_move);
			
			isWalk(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "jump") {	
			isJump(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "jumpPunch") {
			isJumpPunch(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "jumpKick") {
			isJumpKick(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "block") {
			p->getHurtBox()->setBlock(p->getAnimation()->getSprite());
			isBlock(p);
		}
		else if (p->getCharacterSM().current.getStateName() == "crouch") {
			p->getHurtBox()->setCrouch(p->getAnimation()->getSprite());
			isCrouch(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "crouchPunch") {
			p->getHurtBox()->setCrouch(p->getAnimation()->getSprite());
			isCrouchPunch(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "crouchKick") {
			p->getHurtBox()->setCrouch(p->getAnimation()->getSprite());
			isCrouchPunch(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "lightPunch") {
			isLightPunch(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "hardPunch") {
			isHardPunch(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "lightKick") {
			isLightKick(p);
		}

		else if (p->getCharacterSM().current.getStateName() == "hardKick") {
			isHardKick(p);
		}

		p->animSetValues();
	}
}

void Arena::draw()
{
	sf::Texture texture;
	sf::Sprite background;
	if (! texture.loadFromFile("../images/Suzaku-Castle-SFV.jpg")) {
		throw "not working";
	}
	background.setTexture(texture);
	background.setScale({ 2,2 });
	mWindow.draw(background);
	

	for (auto p : playerList) {
		sf::RectangleShape rectangle;
		HitBox* hitBox = p->getHitBox();
		hitBox->setCrouchKick(p->getAnimation()->getSprite());
		rectangle.setSize({ (float)hitBox->getWidth(),(float)hitBox->getHeight() });
		rectangle.setPosition({ (float)hitBox->getLeft() ,(float)hitBox->getTop() });
		rectangle.setFillColor(sf::Color::Red);
		mWindow.draw(rectangle);
		p->getAnimation()->update(mWindow);
		
	}
}

void Arena::isIdle(Player* player)
{	// rightDpad
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick ) {
		player->getAnimation()->setFlipped(false);
		player->getCharacterSM().process(StateManager::c_move);
	}

	else if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick){
		player->getAnimation()->setFlipped(true);
		player->getCharacterSM().process(StateManager::c_move);
	}

	// Jump
	else if (player->getLastMove() == (int)ButtonMap::upDpad || player->getLastMove() == (int)ButtonMap::upJoyStick) {
		player->getCharacterSM().process(StateManager::c_jump);
	}
	// Crouch
	else if (player->getLastMove() == (int)ButtonMap::downDpad || player->getLastMove() == (int)ButtonMap::downJoyStick) {
		player->getCharacterSM().process(StateManager::c_crouch);
	}
	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}
	// Light Punch
	else if (player->getLastMove() == (int)ButtonMap::square) {
		player->getCharacterSM().process(StateManager::c_lightPunch);
	}
	// Hard Punch
	else if (player->getLastMove() == (int)ButtonMap::triangle) {
		player->getCharacterSM().process(StateManager::c_hardPunch); // doesnt work dont know why
	}
	// Light Kick
	else if (player->getLastMove() == (int)ButtonMap::x) {
		player->getCharacterSM().process(StateManager::c_lightKick);
	}
	// Hard Kick
	else if (player->getLastMove() == (int)ButtonMap::circle) {
		player->getCharacterSM().process(StateManager::c_hardKick);
	}
}

void Arena::isWalk(Player* player)
{	// Walk right
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		player->getAnimation()->setFlipped(false);
	}
	// Walk left
	if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		player->getAnimation()->setFlipped(true);
	}
	// Jump
	if (player->getLastMove() == (int)ButtonMap::upDpad || player->getLastMove() == (int)ButtonMap::upJoyStick) {
		player->getCharacterSM().process(StateManager::c_jump);
	}
	// Crouch
	else if (player->getLastMove() == (int)ButtonMap::downDpad || player->getLastMove() == (int)ButtonMap::downJoyStick) {
		player->getCharacterSM().process(StateManager::c_crouch);
	}
	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}
	// Light Punch
	else if (player->getLastMove() == (int)ButtonMap::square) {
		player->getCharacterSM().process(StateManager::c_lightPunch);
	}
	// Hard Punch
	else if (player->getLastMove() == (int)ButtonMap::triangle) {
		player->getCharacterSM().process(StateManager::c_hardPunch); // doesnt work dont know why
	}
	// Light Kick
	else if (player->getLastMove() == (int)ButtonMap::x) {
		player->getCharacterSM().process(StateManager::c_lightKick);
	}
	// Hard Kick
	else if (player->getLastMove() == (int)ButtonMap::circle) {
		player->getCharacterSM().process(StateManager::c_hardKick);
	}
	else if (player->getLastMove() == -1) {
		player->getCharacterSM().process(StateManager::c_idle);
	}
}

void Arena::isJump(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::upDpad || player->getLastMove() == (int)ButtonMap::upJoyStick) {
		

		if (jumpHeight + 5 < maxJumpHeight) {
			jumpHeight += 5;
			player->getAnimation()->setPosY(player->getAnimation()->getPosY() - 5);
		
		}

		else if (jumpHeight < 2 * maxJumpHeight && player->getAnimation()->getPosY() != 500) {
			
			jumpHeight += 5;
			std::cout << player->getAnimation()->getPosY() << std::endl;
			player->getAnimation()->setPosY(player->getAnimation()->getPosY() +5);
		}
		else {
			jumpHeight = 0;
		}
	}

	else if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		player->getAnimation()->setFlipped(false);
		player->getCharacterSM().process(StateManager::c_move);
	}

	else if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		player->getAnimation()->setFlipped(true);
		player->getCharacterSM().process(StateManager::c_move);
	}

	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}

	// Jump Punch
	else if (player->getLastMove() == (int)ButtonMap::square || player->getLastMove() == (int)ButtonMap::triangle) {
		player->getCharacterSM().process(StateManager::c_jumpPunch);
	}
	
	// Jump Kick
	else if (player->getLastMove() == (int)ButtonMap::x || player->getLastMove() == (int)ButtonMap::circle){
		player->getCharacterSM().process(StateManager::c_jumpKick);
	}

	else if (player->getClock().getElapsedTime().asMilliseconds() > 500) {
		player->getAnimation()->setPosY(500);
		player->getCharacterSM().process(StateManager::c_idle);
		player->getClock().restart();
	}
}


void Arena::isJumpPunch(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		player->getAnimation()->setFlipped(false);
		player->getCharacterSM().process(StateManager::c_move);
	}

	else if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		player->getAnimation()->setFlipped(true);
		player->getCharacterSM().process(StateManager::c_move);
	}

	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}

	else if (player->getClock().getElapsedTime().asMilliseconds() > 500) {
		player->getCharacterSM().process(StateManager::c_idle);
		player->getClock().restart();
	}
	
}

void Arena::isJumpKick(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		player->getAnimation()->setFlipped(false);
		player->getCharacterSM().process(StateManager::c_move);
	}

	else if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		player->getAnimation()->setFlipped(true);
		player->getCharacterSM().process(StateManager::c_move);
	}

	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}

	else if (player->getClock().getElapsedTime().asMilliseconds() > 100) {
		player->getCharacterSM().process(StateManager::c_idle);
		player->getClock().restart();
	}
}

void Arena::isBlock(Player* player)
{

	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		player->getAnimation()->setFlipped(false);
		player->getCharacterSM().process(StateManager::c_move);
	}

	else if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		player->getAnimation()->setFlipped(true);
		player->getCharacterSM().process(StateManager::c_move);
	}


	// Crouch Punch
	else if (player->getLastMove() == (int)ButtonMap::square || player->getLastMove() == (int)ButtonMap::triangle) {
		player->getCharacterSM().process(StateManager::c_crouchPunch);
	}

	// Crouch Kick
	else if (player->getLastMove() == (int)ButtonMap::x || player->getLastMove() == (int)ButtonMap::circle) {
		player->getCharacterSM().process(StateManager::c_crouchKick);
	}

	else if (player->getLastMove() == -1) {
		player->getCharacterSM().process(StateManager::c_idle);
	}

}

void Arena::isCrouch(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		player->getAnimation()->setFlipped(false);
		player->getCharacterSM().process(StateManager::c_move);
	}

	else if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		player->getAnimation()->setFlipped(true);
		player->getCharacterSM().process(StateManager::c_move);
	}

	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}

	// Crouch Punch
	else if (player->getLastMove() == (int)ButtonMap::square || player->getLastMove() == (int)ButtonMap::triangle) {
		player->getCharacterSM().process(StateManager::c_crouchPunch);
	}

	// Crouch Kick
	else if (player->getLastMove() == (int)ButtonMap::x || player->getLastMove() == (int)ButtonMap::circle) {
		player->getCharacterSM().process(StateManager::c_crouchKick);
	}

	else if (player->getLastMove() == -1) {
		player->getCharacterSM().process(StateManager::c_idle);
	}
	
}

void Arena::isCrouchPunch(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
	player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}

	else if (player->getClock().getElapsedTime().asMilliseconds() > 100) {
		player->getCharacterSM().process(StateManager::c_crouch);
		player->getClock().restart();
	}
	
}

void Arena::isCrouchKick(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}

	else if (player->getClock().getElapsedTime().asMilliseconds() > 100) {
		player->getCharacterSM().process(StateManager::c_crouch);
		player->getClock().restart();
	}
}



void Arena::isLightPunch(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Walk left
	if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Jump
	if (player->getLastMove() == (int)ButtonMap::upDpad || player->getLastMove() == (int)ButtonMap::upJoyStick) {
		player->getCharacterSM().process(StateManager::c_jump);
	}
	// Crouch
	else if (player->getLastMove() == (int)ButtonMap::downDpad || player->getLastMove() == (int)ButtonMap::downJoyStick) {
		player->getCharacterSM().process(StateManager::c_crouch);
	}
	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}
	else if (player->getLastMove() == -1) {
		player->getCharacterSM().process(StateManager::c_idle);
	}
}

void Arena::isHardPunch(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Walk left
	if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Jump
	if (player->getLastMove() == (int)ButtonMap::upDpad || player->getLastMove() == (int)ButtonMap::upJoyStick) {
		player->getCharacterSM().process(StateManager::c_jump);
	}
	// Crouch
	else if (player->getLastMove() == (int)ButtonMap::downDpad || player->getLastMove() == (int)ButtonMap::downJoyStick) {
		player->getCharacterSM().process(StateManager::c_crouch);
	}
	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}
	else if (player->getLastMove() == -1) {
		player->getCharacterSM().process(StateManager::c_idle);
	}
}

void Arena::isLightKick(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Walk left
	if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Jump
	if (player->getLastMove() == (int)ButtonMap::upDpad || player->getLastMove() == (int)ButtonMap::upJoyStick) {
		player->getCharacterSM().process(StateManager::c_jump);
	}
	// Crouch
	else if (player->getLastMove() == (int)ButtonMap::downDpad || player->getLastMove() == (int)ButtonMap::downJoyStick) {
		player->getCharacterSM().process(StateManager::c_crouch);
	}
	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}
	else if (player->getLastMove() == -1) {
		player->getCharacterSM().process(StateManager::c_idle);
	}
}

void Arena::isHardKick(Player* player)
{
	if (player->getLastMove() == (int)ButtonMap::rightDpad || player->getLastMove() == (int)ButtonMap::rightJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Walk left
	if (player->getLastMove() == (int)ButtonMap::leftDpad || player->getLastMove() == (int)ButtonMap::leftJoyStick) {
		// Dont forget to add scaling
		player->getCharacterSM().process(StateManager::c_move);
	}
	// Jump
	if (player->getLastMove() == (int)ButtonMap::upDpad || player->getLastMove() == (int)ButtonMap::upJoyStick) {
		player->getCharacterSM().process(StateManager::c_jump);
	}
	// Crouch
	else if (player->getLastMove() == (int)ButtonMap::downDpad || player->getLastMove() == (int)ButtonMap::downJoyStick) {
		player->getCharacterSM().process(StateManager::c_crouch);
	}
	// Block
	else if (player->getLastMove() == (int)ButtonMap::leftTrigger || player->getLastMove() == (int)ButtonMap::rightTrigger) {
		player->getCharacterSM().process(StateManager::c_block); // doesnt work dont know why
	}
	else if (player->getLastMove() == -1) {
		player->getCharacterSM().process(StateManager::c_idle);
	}
}

void Arena::isCombo(Player* player)
{
	
}

void Arena::isHadoken(Player* player)
{
	
}

void Arena::isHit(Player* player)
{
	
}

void Arena::isCrouchHit(Player* player)
{

}

void Arena::isFaceHit(Player* player)
{
	
}

void Arena::isKnockDown(Player* player)
{
	
}

void Arena::isKO(Player* player)
{
	
}

void Arena::isTimeOver(Player* player)
{
	
}

void Arena::isVictory(Player* player)
{
	
}
