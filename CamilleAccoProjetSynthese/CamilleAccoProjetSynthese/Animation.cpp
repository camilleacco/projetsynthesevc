#include "Animation.h"



Animation::Animation(std::string imagePath, size_t columns, size_t rows, double refreshInterval, double scale)
: imagePath(imagePath), columns(columns), rows(rows), refreshInterval(refreshInterval), scale(scale)
{
	if (!texture.loadFromFile(imagePath)) {
		throw "not working";
	}

	setSpriteHeight(texture.getSize());
	setSpriteWidth(texture.getSize());

	setRealHeight();
	setRealWidth();

	intrect = sf::IntRect(0, 0,spriteWidth, spriteHeight);
	setInitialPos(300,500);
}

void Animation::setFlipped(bool flipped)
{
	this->flipped = flipped;
}

bool Animation::getFlipped()
{
	return flipped;
}

void Animation::setLooped(bool looped)
{
	this->looped = looped;
}

void Animation::setInitialPos(size_t x, size_t y)
{
	setPosX(x);
	setPosY(y);
}

void Animation::setPosX(size_t x)
{
	posx = x;
}

size_t Animation::getPosX()
{
	return posx;
}

void Animation::setPosY(size_t y)
{
	posy = y;
}

size_t Animation::getPosY()
{
	return posy;
}

void Animation::setSpriteHeight(sf::Vector2<size_t> sizevect)
{
	spriteHeight = sizevect.y / rows;
}

void Animation::setSpriteWidth(sf::Vector2<size_t> sizevect)
{
	spriteWidth = sizevect.x / columns;
}

void Animation::changeRow(size_t row)
{
	currentRow = row;
}

void Animation::changeColumn(size_t column)
{
	currentColumn = column;
}

void Animation::changeMinMaxInterval(size_t min, size_t max)
{
	imgAnimationMin = min;
	imgAnimationMax = max;
}

void Animation::resetCol()
{
	currentColumn = imgAnimationMin;
}

void Animation::update(sf::RenderWindow & window)
{
	intrect.top = currentRow * spriteHeight;
	if (clock.getElapsedTime().asMilliseconds() > refreshInterval * 100) {

		if (currentColumn >= imgAnimationMax) {
			resetCol();
		}

		else {
			currentColumn++;
		}
		clock.restart();
	}
	setRealWidth();
	intrect.left = currentColumn * spriteWidth;
	sprite.setPosition(posx, posy);
	sprite.setScale({getRealWidth(),getRealHeight() });
	sprite.setTextureRect(intrect);
	sprite.setTexture(texture);
	
 	window.draw(sprite);
	
}

sf::Sprite Animation::getSprite()
{
	return sprite;
}

void Animation::setRealHeight()
{
	realHeight = scale;
}

float Animation::getRealHeight()
{
	return realHeight;
}

void Animation::setRealWidth()
{
	if (flipped) {
		realWidth = -scale;
	}
	else {
		realWidth = scale;
	}
}

float Animation::getRealWidth()
{
	return realWidth;
}


