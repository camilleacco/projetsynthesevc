#include "CharacterSelection.h"
#include "ButtonMap.h"


CharacterSelection::CharacterSelection(size_t const & width, size_t const & height, sf::Font const & font, std::vector<Player*>& listPlayer, sf::RenderWindow& window) : Surface(width, height, font, listPlayer, window)
{
	fill();
}

void CharacterSelection::fill()
{
	size_t i = 0;
	for (auto img : spriteImg) {

		sf::Sprite sprite;

		sprite.setPosition(sf::Vector2f(i * 200 + 150, mWindow.getSize().y / 2));
		i++;
		spriteList.push_back(sprite);
	}
}

void CharacterSelection::draw()
{
	size_t i = 0;
	for (auto sprite : spriteList) {
		sf::Texture texture;
		if (!texture.loadFromFile(spriteImg[i])) {
			throw "not working";
		}

		sprite.setTexture(texture);
		mWindow.draw(sprite);
		i++;
	}
}

void CharacterSelection::moveLeft() {
	for (auto p : playerList) {
		if (playerList.at(0) == p) {
			if (select1 - 1 >= 0) {
				select1--;
			}

			else {
				select1 = maxSprite - 1;
			}
		}

		else {
			if (select2 - 1 >= 0) {
				select2--;
			}

			else {
				select2 = maxSprite - 1;
			}
		}
	}
}

void CharacterSelection::moveRight() {
	for (auto p : playerList) {
		if (playerList.at(0) == p) {
			if (select1 + 1 < maxSprite) {
				select1++;
			}

			else {
				select1 = 0;
			}
		}

		else {
			if (select2 + 1 < maxSprite) {
				select2--;
			}

			else {
				select2 = 0;
			}
		}
	}
}

void CharacterSelection::action() {

	for (auto p : playerList) {
		if (p->getLastMove() == (int)ButtonMap::x){


	}
		else if (p->getClock().getElapsedTime().asMilliseconds() > 100) {
			if (p->getLastMove() == (int)ButtonMap::downDpad || p->getLastMove()== (int)ButtonMap::downJoyStick) {
				moveLeft();
			}

			else if (p->getLastMove() == (int)ButtonMap::upDpad || p->getLastMove() == (int)ButtonMap::upJoyStick) {
				moveRight();
			}
			p->getClock().restart();
		}
	}
	
}

void CharacterSelection::findCharac(Player* p) {
	/*if (p == playerList.at(0)) {
		switch (select1)
		{
		case 0: p->

		}
	}*/
}

