#ifndef TRANSITION_H
#define TRANSITION_H

#include "StateManager.h"
class State;

class Transition
{
public:
	Transition(StateManager mTransitionEventt);
	~Transition() = default;

	bool isTransitioning(StateManager const & event);
	State * getNextState();
	void setTransitionEvent(StateManager transitionEvent);
	void setNextState(State * state);

private:
	State* mNextState;
	StateManager mTransitionEvent;
};



#endif // !TRANSITION_H

