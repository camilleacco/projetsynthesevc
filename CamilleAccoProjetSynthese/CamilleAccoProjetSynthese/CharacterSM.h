#ifndef CHARACTERSM_H
#define CHARACTERSM_H

#include "StateMachine.h"

class CharacterSM : public StateMachine
{
public:
	CharacterSM();
	~CharacterSM() = default;

private:
	void initializeSM() override;
	void initializeState() override;
	void initializeStateTransition() override;

	/**************************  STATES  *****************************/
	State* idle = new State("idle");
	State* walk = new State("walk");
	State* jump = new State("jump");
	State* fowardJump = new State("fowardJump");
	State* crouch = new State("crouch");
	State* block = new State("block");
	State* hardPunch = new State("hardPunch");
	State* lightPunch = new State("lightPunch");
	State* hardKick = new State("hardKick");
	State* lightKick = new State("lightKick");
	State* crouchPunch = new State("crouchPunch");
	State* crouchKick = new State("crouchKick");
	State* jumpPunch = new State("jumpPunch");
	State* jumpKick = new State("jumpKick");
	State* hadoken = new State("hadoken");
	State* combo = new State("combo");
	State* hit = new State("hit");
	State* faceHit = new State("faceHit");
	State* crouchHit = new State("crouchHit");
	State* knockdown = new State("knockdown");
	State* kO = new State("kO");
	State* timeOver = new State("timeOver");
	State* victory = new State("victory");

	/**************************  TRANSITIONS  *****************************/

	// Idle transitions
	Transition* wIdle = new Transition(StateManager::c_move); // move left or right
	Transition* jIdle = new Transition(StateManager::c_jump); // jump
	Transition* cIdle = new Transition(StateManager::c_crouch); // crouch
	Transition* bIdle = new Transition(StateManager::c_block); // block
	Transition* hpIdle = new Transition(StateManager::c_hardPunch); // hard punch
	Transition* lpIdle = new Transition(StateManager::c_lightPunch); // light punch
	Transition* hkIdle = new Transition(StateManager::c_hardKick); // hard kick
	Transition* lkIdle = new Transition(StateManager::c_lightKick); // light kick
	Transition* hIdle = new Transition(StateManager::c_hit); // being hit
	Transition* fIdle = new Transition(StateManager::c_facehit); // being facehit
	Transition* kOIdle = new Transition(StateManager::c_ko); // KO
	Transition* tOIdle = new Transition(StateManager::c_timeOver); // time over

	// Walk transitions
	Transition* iWalk = new Transition(StateManager::c_idle); //  idle
	Transition* jWalk = new Transition(StateManager::c_jump); // jump
	Transition* cWalk = new Transition(StateManager::c_crouch); // crouch
	Transition* bWalk = new Transition(StateManager::c_block); // block
	Transition* hpWalk = new Transition(StateManager::c_hardPunch); // hard punch
	Transition* lpWalk = new Transition(StateManager::c_lightPunch); // light punch
	Transition* hkWalk = new Transition(StateManager::c_hardKick); // hard kick
	Transition* lkWalk = new Transition(StateManager::c_lightKick); // light kick
	Transition* hWalk = new Transition(StateManager::c_hit); // being hit
	Transition* fWalk = new Transition(StateManager::c_facehit); // being facehit
	Transition* kOWalk = new Transition(StateManager::c_ko); // KO
	Transition* tOWalk = new Transition(StateManager::c_timeOver); // time over 

	// Jump transitions
	Transition* iJump = new Transition(StateManager::c_idle); // idle
	Transition* fJump = new Transition(StateManager::c_move); // foward jump
	//Transition* cJump = new Transition(StateManager::c_crouch); // crouch
	Transition* bJump = new Transition(StateManager::c_block); // block
	Transition* pJump = new Transition(StateManager::c_jumpPunch); // jump punch
	Transition* kJump = new Transition(StateManager::c_jumpKick); // jump kick
	Transition* hJump = new Transition(StateManager::c_hit); // being hit
	Transition* kOJump = new Transition(StateManager::c_ko); // KO
	Transition* tOJump = new Transition(StateManager::c_timeOver); // time over

	// FowardJump
	Transition* ifJump = new Transition(StateManager::c_idle); //  idle
	Transition* wfJump = new Transition(StateManager::c_move); // walk
	Transition* jfJump = new Transition(StateManager::c_jump); // jump
	//Transition* cfJump = new Transition(StateManager::c_crouch); // crouch
	Transition* bfJump = new Transition(StateManager::c_block); // block
	//Transition* hpfJump = new Transition(StateManager::c_hardPunch); // hard punch
	//Transition* lpfJump = new Transition(StateManager::c_lightPunch); // light punch
	//Transition* hkfJump = new Transition(StateManager::c_hardKick); // hard kick
	//Transition* lkfJump = new Transition(StateManager::c_lightKick); // light kick
	Transition* hfJump = new Transition(StateManager::c_hit); // being hit
	Transition* ffJump = new Transition(StateManager::c_facehit); // being facehit
	Transition* kOfJump = new Transition(StateManager::c_ko); // KO
	Transition* tOfJump = new Transition(StateManager::c_timeOver); // time over

	// Crouch
	Transition* iCrouch = new Transition(StateManager::c_idle); //  idle
	Transition* wCrouch = new Transition(StateManager::c_move); // walk
	//Transition* jCrouch = new Transition(StateManager::c_jump); // jump
	Transition* bCrouch = new Transition(StateManager::c_block); // block
	Transition* pCrouch = new Transition(StateManager::c_crouchPunch); // crouch punch
	Transition* kCrouch = new Transition(StateManager::c_crouchKick); // crouch kick
	Transition* hCrouch = new Transition(StateManager::c_crouchhit); // being crouch hit
	Transition* kOCrouch = new Transition(StateManager::c_ko); // KO
	Transition* tOCrouch = new Transition(StateManager::c_timeOver); // time over

	//Block
	Transition* iBlock = new Transition(StateManager::c_idle); //  idle
	Transition* wBlock = new Transition(StateManager::c_move); // walk
	Transition* jBlock = new Transition(StateManager::c_jump); // jump
	Transition* pBlock = new Transition(StateManager::c_crouchPunch); // crouch punch
	Transition* kBlock = new Transition(StateManager::c_crouchKick); // crouch kick
	Transition* tOBlock = new Transition(StateManager::c_timeOver); // time over

	// Hard Punch
	Transition* iHardPunch = new Transition(StateManager::c_idle); //  idle
	Transition* wHardPunch = new Transition(StateManager::c_move); // walk
	Transition* jHardPunch = new Transition(StateManager::c_jump); // jump
	Transition* cHardPunch = new Transition(StateManager::c_crouch); // crouch
	Transition* bHardPunch = new Transition(StateManager::c_block); // block
	Transition* tOHardPunch = new Transition(StateManager::c_timeOver); // time over
	Transition* vHardPunch = new Transition(StateManager::c_victory); // victory

	// Light punch
	Transition* iLightPunch = new Transition(StateManager::c_idle); //  idle
	Transition* wLightPunch = new Transition(StateManager::c_move); // walk
	Transition* jLightPunch = new Transition(StateManager::c_jump); // jump
	Transition* cLightPunch = new Transition(StateManager::c_crouch); // crouch
	Transition* bLightPunch = new Transition(StateManager::c_block); // block
	Transition* tOLightPunch = new Transition(StateManager::c_timeOver); // time over
	Transition* vLightPunch = new Transition(StateManager::c_victory); // victory
	
	// Hard kick
	Transition* iHardKick = new Transition(StateManager::c_idle); //  idle
	Transition* wHardKick = new Transition(StateManager::c_move); // walk
	Transition* jHardKick = new Transition(StateManager::c_jump); // jump
	Transition* cHardKick = new Transition(StateManager::c_crouch); // crouch
	Transition* bHardKick = new Transition(StateManager::c_block); // block
	Transition* tOHardKick = new Transition(StateManager::c_timeOver); // time over
	Transition* vHardKick = new Transition(StateManager::c_victory); // victory

	// Light kick
	Transition* iLightKick = new Transition(StateManager::c_idle); //  idle
	Transition* wLightKick = new Transition(StateManager::c_move); // walk
	Transition* jLightKick = new Transition(StateManager::c_jump); // jump
	Transition* cLightKick = new Transition(StateManager::c_crouch); // crouch
	Transition* bLightKick = new Transition(StateManager::c_block); // block
	Transition* tOLightKick = new Transition(StateManager::c_timeOver); // time over
	Transition* vLightKick = new Transition(StateManager::c_victory); // victory

	// Crouch punch
	Transition* cCrouchPunch = new Transition(StateManager::c_crouch); // crouch
	Transition* bCrouchPunch = new Transition(StateManager::c_block); // block
	Transition* tOCrouchPunch = new Transition(StateManager::c_timeOver); // time over
	Transition* vCrouchPunch = new Transition(StateManager::c_victory); // victory

	// Crouch kick
	Transition* cCrouchKick = new Transition(StateManager::c_crouch); // crouch
	Transition* bCrouchKick = new Transition(StateManager::c_block); // block
	Transition* tOCrouchKick = new Transition(StateManager::c_timeOver); // time over
	Transition* vCrouchKick = new Transition(StateManager::c_victory); // victory

	// Jump punch
	Transition* iJumpPunch = new Transition(StateManager::c_idle); // idle
	Transition* wJumpPunch = new Transition(StateManager::c_move); // walk
	Transition* bJumpPunch = new Transition(StateManager::c_block); // block
	Transition* tOJumpPunch = new Transition(StateManager::c_timeOver); // time over
	Transition* vJumpPunch = new Transition(StateManager::c_victory); // victory

	// Jump kick
	Transition* iJumpKick = new Transition(StateManager::c_idle); // idle
	Transition* wJumpKick = new Transition(StateManager::c_move); // walk
	Transition* bJumpKick = new Transition(StateManager::c_block); // block
	Transition* tOJumpKick = new Transition(StateManager::c_timeOver); // time over
	Transition* vJumpKick = new Transition(StateManager::c_victory); // victory

	// Hadoken
	Transition* iHadoken = new Transition(StateManager::c_idle); //  idle
	Transition* wHadoken = new Transition(StateManager::c_move); // walk
	Transition* jHadoken = new Transition(StateManager::c_jump); // jump
	Transition* cHadoken = new Transition(StateManager::c_crouch); // crouch
	Transition* bHadoken = new Transition(StateManager::c_block); // block
	Transition* tOHadoken = new Transition(StateManager::c_timeOver); // time over
	Transition* vHadoken = new Transition(StateManager::c_victory); // victory

	// Combo
	Transition* iCombo = new Transition(StateManager::c_idle); //  idle
	Transition* wCombo = new Transition(StateManager::c_move); // walk
	Transition* jCombo = new Transition(StateManager::c_jump); // jump
	Transition* cCombo = new Transition(StateManager::c_crouch); // crouch
	Transition* bCombo = new Transition(StateManager::c_block); // block
	Transition* tOCombo = new Transition(StateManager::c_timeOver); // time over
	Transition* vCombo = new Transition(StateManager::c_victory); // victory

	// Hit
	Transition* iHit = new Transition(StateManager::c_idle); // idle
	Transition* kdHit = new Transition(StateManager::c_knockdown); // knockdown
	Transition* kOHit = new Transition(StateManager::c_ko); // KO
	// Face hit
	Transition* iFaceHit = new Transition(StateManager::c_idle); // idle
	Transition* kdFaceHit = new Transition(StateManager::c_knockdown); // knockdown
	Transition* kOFaceHit = new Transition(StateManager::c_ko); // KO
	// Crouch hit
	Transition* cCrouchHit = new Transition(StateManager::c_crouch); // crouch
	Transition* kdCrouchHit = new Transition(StateManager::c_knockdown); // knockdown
	Transition* kOCrouchHit = new Transition(StateManager::c_ko); // KO
	
	// Knockdown
	Transition* iKnockDown = new Transition(StateManager::c_idle); // idle
	Transition* kOKnockDown = new Transition(StateManager::c_ko); // KO

};

#endif // !CHARACTERSM_H