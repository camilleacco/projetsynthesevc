#ifndef HURTBOX_H
#define HURTBOX_H

#include "Box.h"

class HurtBox : public Box
{
public:
	HurtBox() = default;
	~HurtBox() = default;
	void setNormal(const sf::Sprite & sprite);
	void setCrouch(const sf::Sprite & sprite);
	void setBlock(const sf::Sprite & sprite);
};

#endif // !HURTBOX_H

