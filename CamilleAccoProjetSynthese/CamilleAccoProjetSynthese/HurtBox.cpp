#include "HurtBox.h"

void HurtBox::setNormal(const sf::Sprite& sprite) {
	setHeight(sprite.getGlobalBounds().height * 0.6);
	setWidth(sprite.getGlobalBounds().width *0.7);
	setLeft(sprite.getGlobalBounds().left + width * 0.2);
	setTop(sprite.getGlobalBounds().top + height * 0.7);
}
void HurtBox::setCrouch(const sf::Sprite& sprite) {
	setHeight(sprite.getGlobalBounds().height * 0.4);
	setWidth(sprite.getGlobalBounds().width *0.7);
	setLeft(sprite.getGlobalBounds().left + width * 0.2);
	setTop(sprite.getGlobalBounds().top + height * 1.5);
}
void HurtBox::setBlock(const sf::Sprite& sprite) {
	setHeight(sprite.getGlobalBounds().height * 0.6);
	setWidth(sprite.getGlobalBounds().width / 5);
	setLeft(sprite.getGlobalBounds().left + width);
	setTop(sprite.getGlobalBounds().top + height * 0.7);
}
