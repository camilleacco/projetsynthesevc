#ifndef SPRITEDATA_H
#define SPRITEDATA_H

struct SpriteData
{
	size_t description, row, colMax;
};
#endif SPRITEDATA_H
