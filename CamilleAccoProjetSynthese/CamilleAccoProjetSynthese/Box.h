#ifndef BOX_H
#define BOX_H

#include "SFML/Graphics.hpp"

class Box
{
public:
	Box() = default;
	~Box() = default;

	virtual void setHeight(size_t h);
	virtual size_t getHeight();
	virtual void setWidth(size_t w);
	virtual size_t getWidth();
	virtual void setLeft(size_t x);
	virtual size_t getLeft();
	virtual void setTop(size_t y);
	virtual size_t getTop();




protected:
	size_t height, width, left, top;
};

#endif // !BOX_H

