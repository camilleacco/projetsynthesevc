#ifndef HITBOX_H
#define HITBOX_H

#include "Box.h"

class HitBox : public Box 
{
public:
	HitBox() = default;
	~HitBox() = default;

	void setHardPunchLightKick(const sf::Sprite & sprite);
	void setHardKickLightPunch(const sf::Sprite & sprite);
	void setCrouchPunch(const sf::Sprite & sprite);
	void setCrouchKick(const sf::Sprite & sprite);
};

#endif // !HITBOX_H

