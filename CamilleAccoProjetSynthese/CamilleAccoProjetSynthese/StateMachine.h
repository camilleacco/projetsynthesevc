#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "State.h"

class StateMachine
{
public:
	StateMachine() = default;
	~StateMachine() = default;
	State initial;
	State current;
	void process(StateManager event);
	void addState(State* state);
	std::vector<State*> getStates();

private:
	std::vector<State*> states;
	void initialize();

protected:
	virtual void initializeSM() = 0;
	virtual void initializeState() = 0;
	virtual void initializeStateTransition() = 0;

};



#endif // !STATEMACHINE_H

