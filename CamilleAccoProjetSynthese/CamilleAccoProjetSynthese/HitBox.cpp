#include "HitBox.h"




void HitBox::setHardPunchLightKick(const sf::Sprite & sprite)
{
	setHeight(sprite.getGlobalBounds().height * 0.095);
	setWidth(sprite.getGlobalBounds().width /2);
	setLeft(sprite.getGlobalBounds().left + width);
	setTop(sprite.getGlobalBounds().top + height *6);
}

void HitBox::setHardKickLightPunch(const sf::Sprite & sprite)
{
	setHeight(sprite.getGlobalBounds().height/8);
	setWidth(sprite.getGlobalBounds().width / 2);
	setLeft(sprite.getGlobalBounds().left + width);
	setTop(sprite.getGlobalBounds().top + height *3.5);
}

void HitBox::setCrouchPunch(const sf::Sprite & sprite)
{
	setHeight(sprite.getGlobalBounds().height /8);
	setWidth(sprite.getGlobalBounds().width *0.40);
	setLeft(sprite.getGlobalBounds().left + width /0.80);
	setTop(sprite.getGlobalBounds().top + height * 7);
}

void HitBox::setCrouchKick(const sf::Sprite & sprite)
{
	setHeight(sprite.getGlobalBounds().height / 8);
	setWidth(sprite.getGlobalBounds().width *0.40);
	setLeft(sprite.getGlobalBounds().left + width / 0.80);
	setTop(sprite.getGlobalBounds().top + height * 5.5);
}
