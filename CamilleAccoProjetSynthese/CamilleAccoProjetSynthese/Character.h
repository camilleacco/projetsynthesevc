#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
#include "Animation.h"

class Character
{
public:
	Character(std::string image, double speed, size_t columns, size_t rows, size_t size, size_t strenght, size_t defense);
	~Character() = default;
	virtual void setImg(std::string i);
	virtual std::string getImg();
	virtual void setSize(size_t size);
	virtual size_t getSize();
	virtual void setColumns(size_t col);
	virtual size_t getColumns();
	virtual void setRows(size_t rows);
	virtual size_t getRows();
	virtual Animation* getAnim();
	virtual void setSpeed(double sp);
	virtual double getSpeed();
	virtual void setStrenght(size_t st);
	virtual size_t getStrenght();
	virtual void setDefense(size_t d);
	virtual size_t getDefense();

private:
	std::string image;
	size_t strenght, defense;
	double rows, columns, size, speed;
	Animation* anim;


};

#endif // !CHARACTER_H

