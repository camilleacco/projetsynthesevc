#include "Transition.h"



Transition::Transition(StateManager mTransitionEvent)
	:mTransitionEvent(mTransitionEvent) {}

bool Transition::isTransitioning(StateManager const & event) 
{
	return mTransitionEvent == event;
}

State * Transition::getNextState()
{
	return mNextState;
}

void Transition::setTransitionEvent(StateManager transitionEvent)
{
	mTransitionEvent = transitionEvent;
}

void Transition::setNextState(State * state)
{
	mNextState = state;
}

