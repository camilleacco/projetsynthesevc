#include "CharacterSM.h"

CharacterSM::CharacterSM()
{
	initializeSM();
}


void CharacterSM::initializeSM()
{
	this->initializeState();
	this->initializeStateTransition();
	this->initial = *this->idle;
	this->current = *this->idle;
}

void CharacterSM::initializeState()
{
	this->addState(idle);
	this->addState(walk);
	this->addState(jump);
	this->addState(fowardJump);
	this->addState(crouch);
	this->addState(block);
	this->addState(hardPunch);
	this->addState(lightPunch);
	this->addState(hardKick);
	this->addState(lightKick);
	this->addState(crouchPunch);
	this->addState(crouchKick);
	this->addState(jumpPunch);
	this->addState(jumpKick);
	this->addState(hadoken);
	this->addState(combo);
	this->addState(hit);
	this->addState(faceHit);
	this->addState(crouchHit);
	this->addState(knockdown);
	this->addState(kO);
	this->addState(timeOver);
	this->addState(victory);
}

void CharacterSM::initializeStateTransition()
{
/*********STARTING FROM IDLE*******/
	// TO WALK
	idle->addTransition(wIdle);
	wIdle->setNextState(walk);

	// TO JUMP
	idle->addTransition(jIdle);
	jIdle->setNextState(jump);

	// TO CROUCH
	idle->addTransition(cIdle);
	cIdle->setNextState(crouch);

	// TO BLOCK
	idle->addTransition(bIdle);
	bIdle->setNextState(block);

	// TO HARD PUNCH
	idle->addTransition(hpIdle);
	hpIdle->setNextState(hardPunch);

	// TO LIGHT PUNCH
	idle->addTransition(lpIdle);
	lpIdle->setNextState(lightPunch);

	// TO HARD KICK
	idle->addTransition(hkIdle);
	hkIdle->setNextState(hardKick);

	// TO LIGHT KICK
	idle->addTransition(lkIdle);
	lkIdle->setNextState(lightKick);

	// TO HIT
	idle->addTransition(hIdle);
	hIdle->setNextState(hit);

	// TO FACE HIT
	idle->addTransition(fIdle);
	fIdle->setNextState(faceHit);

	// TO KO
	idle->addTransition(kOIdle);
	kOIdle->setNextState(kO);

	// TO TIME OVER
	idle->addTransition(tOIdle);
	tOIdle->setNextState(timeOver);

/*********STARTING FROM WALK*******/
	// TO IDLE
	walk->addTransition(iWalk);
	iWalk->setNextState(idle);

	// TO JUMP
	walk->addTransition(jWalk);
	jWalk->setNextState(jump);

	// TO CROUCH
	walk->addTransition(cWalk);
	cWalk->setNextState(crouch);

	// TO BLOCK
	walk->addTransition(bWalk);
	bWalk->setNextState(block);

	// TO HARD PUNCH
	walk->addTransition(hpWalk);
	hpWalk->setNextState(hardPunch);

	// TO LIGHT PUNCH
	walk->addTransition(lpWalk);
	lpWalk->setNextState(lightPunch);

	// TO HARD KICK
	walk->addTransition(hkWalk);
	hkWalk->setNextState(hardKick);

	// TO LIGHT KICK
	walk->addTransition(lkWalk);
	lkWalk->setNextState(lightKick);

	// TO HIT
	walk->addTransition(hWalk);
	hWalk->setNextState(hit);

	// TO FACE HIT
	walk->addTransition(fWalk);
	fWalk->setNextState(faceHit);

	// TO KO
	walk->addTransition(kOWalk);
	kOWalk->setNextState(kO);

	// TO TIME OVER
	walk->addTransition(tOWalk);
	tOWalk->setNextState(timeOver);

/*********STARTING FROM JUMP*******/
	// TO IDLE
	jump->addTransition(iJump);
	iJump->setNextState(idle);

	// TO FOWARD JUMP
	jump->addTransition(fJump);
	fJump->setNextState(fowardJump);

	// TO CROUCH
	//jump->addTransition(cJump);
	//cJump->setNextState(crouch);

	// TO BLOCK
	jump->addTransition(bJump);
	bJump->setNextState(block);

	// TO JUMP PUNCH
	jump->addTransition(pJump);
	pJump->setNextState(jumpPunch);

	// TO JUMP KICK
	jump->addTransition(kJump);
	kJump->setNextState(jumpKick);

	// TO HIT
	jump->addTransition(hJump);
	hJump->setNextState(hit);

	// TO KO
	jump->addTransition(kOJump);
	kOJump->setNextState(kO);

	// TO TIME OVER
	jump->addTransition(tOJump);
	tOJump->setNextState(timeOver);


/*********STARTING FROM FOWARD JUMP*******/
	// TO IDLE
	fowardJump->addTransition(ifJump);
	ifJump->setNextState(idle);

	// TO WALK
	fowardJump->addTransition(wfJump);
	ifJump->setNextState(idle);

	// TO JUMP
	fowardJump->addTransition(jfJump);
	jfJump->setNextState(jump);

	// TO CROUCH
	//fowardJump->addTransition(cfJump);
	//cfJump->setNextState(crouch);

	// TO BLOCK
	fowardJump->addTransition(bfJump);
	bfJump->setNextState(block);

	// TO HARD PUNCH
	//fowardJump->addTransition(hpfJump);
	//hpfJump->setNextState(hardPunch);

	// TO LIGHT PUNCH
	//fowardJump->addTransition(lpfJump);
	//lpfJump->setNextState(lightPunch);

	// TO HARD KICK
	//fowardJump->addTransition(hkfJump);
	//hkfJump->setNextState(hardKick);

	// TO LIGHT KICK
	//fowardJump->addTransition(lkfJump);
	//lkfJump->setNextState(lightKick);

	// TO HIT
	fowardJump->addTransition(hfJump);
	hfJump->setNextState(hit);

	// TO FACE HIT
	fowardJump->addTransition(ffJump);
	ffJump->setNextState(faceHit);

	// TO KO
	fowardJump->addTransition(kOJump);
	kOJump->setNextState(kO);

	// TO TIME OVER
	fowardJump->addTransition(tOfJump);
	tOfJump->setNextState(timeOver);

/*********STARTING FROM CROUCH*******/
	// TO IDLE
	crouch->addTransition(iCrouch);
	iCrouch->setNextState(idle);

	// TO WALK
	crouch->addTransition(wCrouch);
	wCrouch->setNextState(walk);

	// TO JUMP
	//crouch->addTransition(jCrouch);
	//jCrouch->setNextState(jump);

	// TO BLOCK
	crouch->addTransition(bCrouch);
	bCrouch->setNextState(block);

	// TO CROUCH PUNCH
	crouch->addTransition(pCrouch);
	pCrouch->setNextState(crouchPunch);

	// TO CROUCH KICK
	crouch->addTransition(kCrouch);
	kCrouch->setNextState(crouchKick);

	// TO CROUCH HIT
	crouch->addTransition(kCrouch);
	kCrouch->setNextState(crouchKick);

	// TO KO
	crouch->addTransition(kOCrouch);
	kOCrouch->setNextState(kO);

	// TO TIME OVER
	crouch->addTransition(tOCrouch);
	tOCrouch->setNextState(timeOver);


/*********STARTING FROM BLOCK*******/
	// TO IDLE
	block->addTransition(iBlock);
	iBlock->setNextState(idle);

	// TO WALK
	block->addTransition(wBlock);
	wBlock->setNextState(walk);

	// TO JUMP
	block->addTransition(jBlock);
	jBlock->setNextState(jump);

	// TO CROUCH PUNCH
	block->addTransition(pBlock);
	pBlock->setNextState(crouchPunch);

	// TO CROUCH KICK
	block->addTransition(kBlock);
	kBlock->setNextState(crouchKick);

	// TO TIME OVER
	block->addTransition(tOBlock);
	tOBlock->setNextState(timeOver);


/*********STARTING FROM HARD PUNCH*******/
	// TO IDLE
	hardPunch->addTransition(iHardPunch);
	iHardPunch->setNextState(idle);

	// TO WALK
	hardPunch->addTransition(wHardPunch);
	wHardPunch->setNextState(walk);

	// TO JUMP
	hardPunch->addTransition(jHardPunch);
	jHardPunch->setNextState(jump);
	
	// TO CROUCH
	hardPunch->addTransition(cHardPunch);
	cHardPunch->setNextState(crouch);

	// TO BLOCK
	hardPunch->addTransition(bHardPunch);
	bHardPunch->setNextState(block);

	// TO TIME OVER
	hardPunch->addTransition(tOHardPunch);
	tOHardPunch->setNextState(timeOver);

	// TO VICTORY
	hardPunch->addTransition(vHardPunch);
	vHardPunch->setNextState(victory);


/*********STARTING FROM LIGHT PUNCH*******/
	// TO IDLE
	lightPunch->addTransition(iLightPunch);
	iLightPunch->setNextState(idle);

	// TO WALK
	lightPunch->addTransition(wLightPunch);
	wLightPunch->setNextState(walk);

	// TO JUMP
	lightPunch->addTransition(jLightPunch);
	jLightPunch->setNextState(jump);

	// TO CROUCH
	lightPunch->addTransition(cLightPunch);
	cLightPunch->setNextState(crouch);

	// TO BLOCK
	lightPunch->addTransition(bLightPunch);
	bLightPunch->setNextState(block);

	// TO TIME OVER
	lightPunch->addTransition(bLightPunch);
	bLightPunch->setNextState(block);

	// TO VICTORY
	lightPunch->addTransition(vLightPunch);
	vLightPunch->setNextState(victory);


/*********STARTING FROM HARD KICK*******/
	// TO IDLE
	hardKick->addTransition(iHardKick);
	iHardKick->setNextState(idle);

	// TO WALK
	hardKick->addTransition(wHardKick);
	wHardKick->setNextState(walk);

	// TO JUMP
	hardKick->addTransition(jHardKick);
	jHardKick->setNextState(jump);

	// TO CROUCH
	hardKick->addTransition(cHardKick);
	cHardKick->setNextState(crouch);

	// TO BLOCK
	hardKick->addTransition(bHardKick);
	bHardKick->setNextState(block);

	// TO TIME OVER
	hardKick->addTransition(tOHardKick);
	tOHardKick->setNextState(timeOver);

	// TO VICTORY
	hardKick->addTransition(vHardKick);
	vHardKick->setNextState(victory);


/*********STARTING FROM LIGHT KICK*******/
	// TO IDLE
	lightKick->addTransition(iLightKick);
	iLightKick->setNextState(idle);

	// TO WALK
	lightKick->addTransition(wLightKick);
	wLightKick->setNextState(walk);

	// TO JUMP
	lightKick->addTransition(jLightKick);
	jLightKick->setNextState(jump);

	// TO CROUCH
	lightKick->addTransition(cLightKick);
	cLightKick->setNextState(crouch);

	// TO BLOCK
	lightKick->addTransition(bLightKick);
	bLightKick->setNextState(block);

	// TO TIME OVER
	lightKick->addTransition(tOLightKick);
	tOLightKick->setNextState(timeOver);

	// TO VICTORY
	lightKick->addTransition(vLightKick);
	vLightKick->setNextState(victory);


/*********STARTING FROM CROUCH PUNCH*******/
	// TO CROUCH
	crouchPunch->addTransition(cCrouchPunch);
	cCrouchPunch->setNextState(crouch);

	// TO BLOCK
	crouchPunch->addTransition(bCrouchPunch);
	bCrouchPunch->setNextState(block);

	// TO TIME OVER
	crouchPunch->addTransition(tOCrouchPunch);
	tOCrouchPunch->setNextState(timeOver);

	// TO VICTORY
	crouchPunch->addTransition(vCrouchPunch);
	vCrouchPunch->setNextState(victory);


/*********STARTING FROM CROUCH KICK*******/
	// TO CROUCH
	crouchKick->addTransition(cCrouchKick);
	cCrouchKick->setNextState(crouch);

	// TO BLOCK
	crouchKick->addTransition(bCrouchKick);
	bCrouchKick->setNextState(block);

	// TO TIME OVER
	crouchKick->addTransition(tOCrouchKick);
	tOCrouchKick->setNextState(timeOver);

	// TO VICTORY
	crouchKick->addTransition(vCrouchKick);
	vCrouchKick->setNextState(victory);


/*********STARTING FROM JUMP PUNCH*******/
	// TO IDLE
	jumpPunch->addTransition(iJumpPunch);
	iJumpPunch->setNextState(idle);

	// TO WALK
	jumpPunch->addTransition(wJumpPunch);
	wJumpPunch->setNextState(walk);

	// TO BLOCK
	jumpPunch->addTransition(bJumpPunch);
	bJumpPunch->setNextState(block);

	// TO TIME OVER
	jumpPunch->addTransition(tOJumpPunch);
	tOJumpPunch->setNextState(timeOver);

	// TO VICTORY
	jumpPunch->addTransition(vJumpPunch);
	vJumpPunch->setNextState(victory);


/*********STARTING FROM JUMP KICK*******/
	// TO IDLE
	jumpKick->addTransition(iJumpKick);
	iJumpKick->setNextState(idle);

	// TO WALK
	jumpKick->addTransition(wJumpKick);
	wJumpKick->setNextState(walk);

	// TO BLOCK
	jumpKick->addTransition(bJumpKick);
	bJumpKick->setNextState(block);

	// TO TIME OVER
	jumpKick->addTransition(tOJumpKick);
	tOJumpKick->setNextState(timeOver);

	// TO VICTORY
	jumpKick->addTransition(vJumpKick);
	vJumpKick->setNextState(victory);

/*********STARTING FROM HADOKEN*******/
	// TO IDLE
	hadoken->addTransition(iHadoken);
	iHadoken->setNextState(idle);

	// TO WALK
	hadoken->addTransition(wHadoken);
	wHadoken->setNextState(walk);

	// TO JUMP
	hadoken->addTransition(jHadoken);
	jHadoken->setNextState(jump);

	// TO CROUCH
	hadoken->addTransition(cHadoken);
	cHadoken->setNextState(crouch);

	// TO BLOCK
	hadoken->addTransition(bHadoken);
	bHadoken->setNextState(block);

	// TO TIME OVER
	hadoken->addTransition(tOHadoken);
	tOHadoken->setNextState(timeOver);

	// TO VICTORY
	hadoken->addTransition(vHadoken);
	vHadoken->setNextState(victory);


/*********STARTING FROM COMBO*******/
	// TO IDLE
	combo->addTransition(iCombo);
	iCombo->setNextState(idle);

	// TO WALK
	combo->addTransition(wCombo);
	wCombo->setNextState(walk);

	// TO JUMP
	combo->addTransition(jCombo);
	jCombo->setNextState(jump);

	// TO CROUCH
	combo->addTransition(cCombo);
	cCombo->setNextState(crouch);

	// TO BLOCK
	combo->addTransition(bCombo);
	bCombo->setNextState(block);

	// TO TIME OVER
	combo->addTransition(tOCombo);
	tOCombo->setNextState(timeOver);

	// TO VICTORY
	combo->addTransition(vCombo);
	vCombo->setNextState(victory);

/*********STARTING FROM HIT*******/
	// TO IDLE
	hit->addTransition(iHit);
	iHit->setNextState(idle);

	// TO KNOCKDOWN
	hit->addTransition(kdHit);
	kdHit->setNextState(knockdown);

	// TO KO
	hit->addTransition(kdHit);
	kdHit->setNextState(knockdown);

/*********STARTING FROM FACE HIT*******/
	// TO IDLE
	faceHit->addTransition(iFaceHit);
	iFaceHit->setNextState(idle);
	
	// TO KNOCKDOWN
	faceHit->addTransition(kdFaceHit);
	kdFaceHit->setNextState(knockdown);

	// TO KO
	faceHit->addTransition(kOFaceHit);
	kOFaceHit->setNextState(kO);

/*********STARTING FROM CROUCH HIT*******/

	// TO CROUCH
	crouchHit->addTransition(cCrouchHit);
	cCrouchHit->setNextState(crouch);
	// TO KNOCKDOWN
	crouchHit->addTransition(kdCrouchHit);
	kdCrouchHit->setNextState(knockdown);

	// TO KO
	crouchHit->addTransition(kOCrouchHit);
	kOCrouchHit->setNextState(kO);

/*********STARTING FROM KNOCKDOWN*******/
	// TO IDLE
	knockdown->addTransition(iKnockDown);
	iKnockDown->setNextState(idle);

	// TO KO
	knockdown->addTransition(kOKnockDown);
	kOKnockDown->setNextState(kO);

}
