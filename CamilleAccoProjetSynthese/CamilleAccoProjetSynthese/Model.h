#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include "Player.h"

class Model
{
public:
	Model();
	~Model();

	std::vector<Player*> getPlayerList();
	

private:
	std::vector <Player*> playerList;
};

#endif // !MODEL_H

