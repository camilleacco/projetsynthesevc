#include "MainMenu.h"


MainMenu::MainMenu(size_t const & width, size_t const & height, sf::Font const & font, std::vector<Player*>& listPlayer, sf::RenderWindow& window) : Surface(width, height, font, listPlayer, window)
{
	fill();
}

void MainMenu::fill() {
		size_t i{ 0 };
	if (!buffer.loadFromFile("../audio/openingtitle.wav")) {
		throw "not working";
	}
	sound.setBuffer(buffer);
	sound.play();
	if (!titlefont.loadFromFile("../font/ninjagardenhalf.ttf")) {
		throw "doesnt work";
	}
	
	mainTitle.setFont(titlefont);
	mainTitle.setString("SUPER FIGHTER 3000");
	mainTitle.setFillColor(sf::Color::Yellow);
	mainTitle.setCharacterSize(75);
	mainTitle.setPosition(sf::Vector2f(275, 150));

	for (auto name : menuNames) {
		sf::Text txt;
		txt.setFont(font);
		txt.setString(name);
		if (i ==0) {
			txt.setFillColor(sf::Color::Red); /*First element will be in focus*/
		}
		else {
			txt.setFillColor(sf::Color::White);
		}
		txt.setPosition(sf::Vector2f(width / 2 - 80, ((height-500)/4)*(i+1) + 300));
		i++;
		items.push_back(txt);
	}
}

void MainMenu::moveUp()
{
	items.at(selected).setFillColor(sf::Color::White);

	if (selected -1 >= 0) {
		selected--;
	}
	else {
		selected = maxItems - 1;
	}
	items.at(selected).setFillColor(sf::Color::Red);

}

void MainMenu::moveDown()
{
	items.at(selected).setFillColor(sf::Color::White);
	if (selected + 1 < maxItems) {
		selected++;
	}
	else {
		selected = 0;
	}
	items.at(selected).setFillColor(sf::Color::Red);
}

StateManager MainMenu::selectItem()
{
	int sm = 0;
	std::string focused = items.at(selected).getString();
	if (focused == "Play") {
		return StateManager::w_select;
	}

	else if (focused == "Options") {
		return StateManager::w_options;
	}

	else if (focused == "Highscore") {
		return StateManager::w_highscore;
	}

	else {
		return StateManager::w_quit;
	}
}

void MainMenu::draw()
{
	mWindow.draw(mainTitle);
	for (auto item : items) {
		mWindow.draw(item);
	}
}

void MainMenu::action(GameSM & gsm)
{
		if (playerList.at(0)->getLastMove() == (int)ButtonMap::x) {
			int test = (int)selectItem();
			gsm.process(selectItem());
		}

		else if (clock.getElapsedTime().asMilliseconds() > 100) {
			 if (playerList.at(0)->getLastMove() == (int)ButtonMap::downDpad || playerList.at(0)->getLastMove() == (int)ButtonMap::downJoyStick) {
				moveDown();
			 }

			 else if (playerList.at(0)->getLastMove() == (int)ButtonMap::upDpad || playerList.at(0)->getLastMove() == (int)ButtonMap::upJoyStick) {
				 moveUp();
			 }
			 clock.restart();
		}
}
