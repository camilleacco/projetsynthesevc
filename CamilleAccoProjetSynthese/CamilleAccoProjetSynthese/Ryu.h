#ifndef  RYU_H
#define RYU_H

#include "Character.h"

class Ryu : public Character
{
public:
	Ryu(std::string image = "../images/newRyu.png", double speed = 1.5, size_t columns = 10, size_t rows = 22, size_t size = 1.5, size_t strenght = 100, size_t defense = 100);
	~Ryu();
};

#endif // ! RYU_H

