#ifndef MAINMENU_H
#define MAINMENU_H


#include <vector>
#include <string>
#include "Surface.h"
#include "GameSM.h"
#include "ButtonMap.h"
#include "Player.h"
#define maxItems 4

class MainMenu : public Surface
{
public:
	MainMenu() = default;
	~MainMenu() = default;
	MainMenu(size_t const & width, size_t const & height, sf::Font const & font, std::vector<Player*>& listPlayer, sf::RenderWindow & window);
	void fill() override;
	void moveUp();
	void moveDown();
	StateManager selectItem();
	void draw() override;
	void action(GameSM & gsm);
private:
	int selected{0}; /*selected item in the menu*/
	sf::Text mainTitle;
	sf::Font titlefont;
	std::string menuNames[maxItems] = {"Play", "Options", "Highscore", "Quit"};
	sf::Clock clock;
};

#endif // !MAINMENU_H