#ifndef MBISON_H
#define MBISON_H

#include "Character.h"

class MBison :public Character
{
public:
	MBison(std::string image = "../images/newBison.png", double speed = 0.5, size_t columns = 10, size_t rows = 22, size_t size = 3, size_t strenght = 150, size_t defense = 75);
	~MBison() = default;
};

#endif // !MBISON_H

