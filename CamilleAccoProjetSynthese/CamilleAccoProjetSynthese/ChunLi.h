#ifndef CHUNLI_H

#define CHUNLI_H

#include "Character.h"

class ChunLi :public Character
{

public:
	ChunLi(std::string image = "../images/new-chun-li.png", double speed = 2.0, size_t columns = 10, size_t rows = 22, size_t size = 1, size_t strenght = 50, size_t defense = 30);
	~ChunLi() = default;
};

#endif // !CHUNLI_H

