#ifndef CHARACTERSELECTION_H
#define CHARACTERSELECTION_H

#include "Surface.h"
#include "Player.h"
#include <string>
#include <vector>
#include "SFML/Graphics.hpp"

#define maxSprite 6


class CharacterSelection : public Surface
{
public:
	CharacterSelection() = default;
	CharacterSelection(size_t const &width, size_t const &height, sf::Font const &font, std::vector<Player*>& listPlayer, sf::RenderWindow& window);
	~CharacterSelection() = default;

	void fill() override;
	void draw() override;

	void moveLeft();

	void moveRight();

	void action();

	void findCharac(Player * p);

private:
	std::string spriteImg[maxSprite] = { "../images/ChunLiSquare.jpg", "../images/DhalsimSquare.jpg", "../images/KenSquare.jpg", "../images/RyuSquare.jpg", "../images/ZangiefSquare.jpg", "../images/MBisonSquare.jpg" };
	std::vector<sf::Sprite> spriteList;
	size_t select1, select2;
};

#endif // !CHARACTERSELECTION_H