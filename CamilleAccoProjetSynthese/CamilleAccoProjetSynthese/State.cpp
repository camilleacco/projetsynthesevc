#include "State.h"



State::State(std::string stateName)
	:mStateName(stateName){}

Transition* State::isTransitioning(StateManager & event) {
	for (auto transition : allTransitions) {
		if (transition->isTransitioning(event)) {
			return transition;
		}
	}
	return nullptr;
}

void State::addTransition(Transition * transition) {
	allTransitions.push_back(transition);
}
std::string State::getStateName() {
	return mStateName;
}
void State::setStateName(std::string name) {
	mStateName = name;
}
std::vector<Transition*> State::getTransitions() {
	return allTransitions;
}

