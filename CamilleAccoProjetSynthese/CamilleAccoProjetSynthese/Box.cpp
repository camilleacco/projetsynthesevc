#include "Box.h"




void Box::setHeight(size_t h)
{
	height = h;
}

size_t Box::getHeight()
{
	return height;
}

void Box::setWidth(size_t w)
{
	width = w;
}

size_t Box::getWidth()
{
	return width;
}

void Box::setLeft(size_t x)
{
	left = x;
}

size_t Box::getLeft()
{
	return left;
}

void Box::setTop(size_t y)
{
	top = y;
}

size_t Box::getTop()
{
	return top;
}

