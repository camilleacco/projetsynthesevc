#ifndef BUTTONMAP_H

#define BUTTONMAP_H

enum class ButtonMap {
	defaultval = -1,
	square,
	x,
	circle,
	triangle,
	leftTrigger = 6,
	rightTrigger = 7,
	pause = 9,
	leftJoyStick = 10,
	rightJoyStick = 11,
	upJoyStick = 12,
	downJoyStick = 13,
	leftDpad = 14,
	rightDpad = 15,
	downDpad = 16,
	upDpad = 17
};

#endif //!BUTTONMAP_H
