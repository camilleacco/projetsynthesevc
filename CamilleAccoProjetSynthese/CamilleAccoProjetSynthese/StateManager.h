#ifndef STATEMANAGER_H
#define STATEMANAGER_H

enum class StateManager
{
	/******************* FOR GAME *****************/
	w_select ,
	w_options,
	w_highscore,
	w_back,
	w_quit,
	w_pause,
	w_dead,
	/******************* FOR CHARACTER *****************/
	c_idle,
	c_move,
	c_jump,
	c_crouch,
	c_lightPunch,
	c_hardPunch,
	c_jumpPunch,
	c_crouchPunch,
	c_lightKick,
	c_hardKick,
	c_jumpKick,
	c_crouchKick,
	c_block,
	c_facehit,
	c_hit,
	c_knockdown,
	c_crouchhit,
	c_hadoken,
	c_combo,
	c_timeOver,
	c_ko,
	c_victory
};

#endif // !STATEMANAGER_H

