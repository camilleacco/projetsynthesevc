#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>
#include <SFML/Graphics.hpp>

class Animation
{
public:
	Animation() = default;
	Animation(std::string imagePath, size_t columns, size_t rows,  double refreshInterval, double scale);
	~Animation() = default;

	void setFlipped(bool flipped);
	bool getFlipped();
	void setLooped(bool looped);
	void setInitialPos(size_t x, size_t y);
	void setPosX(size_t x);
	size_t getPosX();
	void setPosY(size_t y);
	size_t getPosY();
	void setSpriteHeight(sf::Vector2 <size_t> sizevect);
	void setSpriteWidth(sf::Vector2 <size_t> sizevect);
	void changeRow(size_t row);
	void changeColumn(size_t column);
	void changeMinMaxInterval(size_t min, size_t max);
	void resetCol();
	void update(sf::RenderWindow &window);
	sf::Sprite getSprite();

private:
	void setRealHeight();
	float getRealHeight();
	void setRealWidth();
	float getRealWidth();

	std::string imagePath;
	size_t columns, rows, currentRow{ 0 }, currentColumn{ 0 }, imgAnimationMin{ 0 }, imgAnimationMax, spriteWidth, spriteHeight{ 0 };
	double scale, refreshInterval;
	float realHeight, realWidth;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Clock clock;
	sf::IntRect intrect;
	size_t posx;
	size_t posy;
	bool flipped{ false }, looped{ true };

};

#endif // !ANIMATION_H

