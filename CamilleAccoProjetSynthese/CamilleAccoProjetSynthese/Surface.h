#ifndef SURFACE_H
#define SURFACE_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include "Player.h"
class Surface
{
public:
	Surface() = default;
	Surface(size_t const &width, size_t const &height, sf::Font const &font, std::vector<Player*>& listPlayer, sf::RenderWindow& window);
	~Surface() = default;
	virtual void fill() = 0;
	virtual void draw() = 0;
	std::vector<sf::Text> items;
	size_t width;
	size_t height;
	

protected:
	sf::Font font;
	sf::SoundBuffer buffer;
	sf::Sound sound;
	sf::RenderWindow& mWindow;
	std::vector<Player*> playerList;

};

#endif // !SURFACE_H

