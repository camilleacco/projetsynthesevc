#ifndef STATE_H
#define STATE_H

#include "Transition.h"
#include <vector>
#include <string>
class State
{
public:
	State() = default;
	State(std::string stateName);
	~State() = default;

	Transition * isTransitioning(StateManager & event);
	void addTransition(Transition * transition);
	std::string getStateName();
	void setStateName(std::string name);
	std::vector<Transition*> getTransitions();

private:
	std::string mStateName;
	std::vector<Transition*> allTransitions;

};

#endif // !STATE_H

