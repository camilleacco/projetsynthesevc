#ifndef DHALSIM_H
#define DHALSIM_H

#include "Character.h"

class Dhalsim : public Character
{
public:
	Dhalsim(std::string image = "../images/newDhalsim.png", double speed = 2.0, size_t columns = 10, size_t rows = 22, size_t size = 1, size_t strenght = 70, size_t defense = 20);
	~Dhalsim() = default;
};

#endif // !DHALSIM_H