#ifndef KEN_H
#define KEN_H

#include "Character.h"

class Ken : public Character
{
public:
	Ken(std::string image = "../images/new-chun-li.png", double speed = 2.0, size_t columns = 10, size_t rows = 22, size_t size = 1, size_t strenght = 50, size_t defense = 30);
	~Ken() = default;
};


#endif // !KEN_H

