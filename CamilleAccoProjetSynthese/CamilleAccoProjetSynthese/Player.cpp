#include "Player.h"
#include "ButtonMap.h"
#include "ChunLi.h"
#include "Dhalsim.h"
#include "Ken.h"
#include "MBison.h"
#include "Ryu.h"
#include <iostream>


using namespace std;
Player::Player(size_t index)
	:index(index)
{
	fillMap();

	
}

void Player::setUpArena() {
	csm = CharacterSM();
	charac = new ChunLi();
	animSetValues();
	hurtBox = new HurtBox();
	hitBox = new HitBox();
}

void Player::setnewChunLi() {
	charac = new ChunLi();
}

void Player::setnewDhalsim() {
	charac = new Dhalsim();
}

void Player::setnewKen() {
	charac = new Ken();
}

void Player::setnewMBison(){
	charac = new MBison();
}

void Player::setnewRyu() {
	charac = new Ryu();
}

void Player::setnewZanglief() {
	/*charac = new Zanglief();*/
}

void Player::animSetValues() {
	int row = std::get<0>(spriteData.find(csm.current.getStateName())->second);
	int max = std::get<1>(spriteData.find(csm.current.getStateName())->second);
	charac->getAnim()->changeRow(row);
	charac->getAnim()->changeMinMaxInterval(0, max);
}

size_t Player::getIndex() {
	return index;
}

void Player::setLife(size_t life) {
	this->life = life;
}

size_t Player::getLife() {
	return life;
}

void Player::isPressed(int move)
{
	lastMove= move;
}

void Player::isMoved(size_t axis, float position)
{
	translateAxis(axis, position);
}

int Player::getLastButton() {
	return lastButton;
}

int Player::getLastMove() {
	return lastMove;
}

void Player::setPoints(size_t points) {
	this->points = points;
}

size_t Player::getPoints() {
	return this->points;
}

void Player::setRagePoints(size_t rageMeter) {
	this->rageMeter = rageMeter;
}

size_t Player::getRagePoints() {
	return this->rageMeter;
}

bool Player::isAlive() {
	return this->alive;
}

bool Player::isInRage() {
	return this->inRage;
}

void Player::attack() {

}

void Player::defend() {

}

Animation * Player::getAnimation()
{
	return charac->getAnim();
}

CharacterSM& Player::getCharacterSM()
{
	return csm;
}

sf::Clock & Player::getClock()
{
	return playerClock;
}

void Player::initialPosition()
{
	if (index % 2 == 0) {
		charac->getAnim()->setPosX(50);
	}
}

HurtBox * Player::getHurtBox()
{
	return hurtBox;
}

HitBox * Player::getHitBox()
{
	return hitBox;
}

void Player::translateAxis(size_t axis, float position)
{
	if (axis == 6) {
		if (position < 0) {
			lastMove =(int) ButtonMap::leftDpad;
		}

		else if (position > 0) {
			lastMove = (int) ButtonMap::rightDpad;
		}

		else if (lastMove == 14 || lastMove == 15) {
			lastMove = -1;
		}
	}

	else if (axis == 7) {
		if (position < 0) {
			lastMove = (int) ButtonMap::downDpad ;
		}

		else if (position > 0) {
			lastMove = (int) ButtonMap::upDpad;
		}

		else if (lastMove == 16 || lastMove ==  17) {
			lastMove = (int) ButtonMap::defaultval;
		}
	}

	else if (axis == 0) {
		if (position < -deadZone) {
			lastMove = (int) ButtonMap::leftJoyStick;
		}

		else if (position > deadZone) {
			lastMove = (int) ButtonMap::rightJoyStick;
		}
		else if (lastMove == 10 || lastMove == 11) {
				lastMove = (int) ButtonMap::defaultval;
			}
		

		
	}

	else if (axis == 1) {
		if (position < -deadZone) {
			lastMove = (int) ButtonMap::upJoyStick;
		}

		else if (position > deadZone) {
			lastMove = (int)ButtonMap::downJoyStick;
		}

		else if (lastMove == 12 || lastMove == 13) {
				lastMove = (int) ButtonMap::defaultval;
			}
	}

	

}

void Player::fillMap()
{
	spriteData["idle"] = std::make_tuple(0, 3);
	spriteData["walk"] = std::make_tuple(1, 7);
	spriteData["jump"] = std::make_tuple(2, 3);
	spriteData["crouch"] = std::make_tuple(3, 1);
	spriteData["lightPunch"] = std::make_tuple(4, 2);
	spriteData["hardPunch"] = std::make_tuple(5, 2);
	spriteData["block"] = std::make_tuple(6, 1);
	spriteData["lightKick"] = std::make_tuple(7, 4);
	spriteData["hardKick"] = std::make_tuple(8, 3);
	spriteData["crouchPunch"] = std::make_tuple(9, 2);
	spriteData["crouchKick"] = std::make_tuple(10, 2);
	spriteData["jumpPunch"] = std::make_tuple(11, 2);
	spriteData["jumpKick"] = std::make_tuple(12, 4);
	spriteData["hadoken"] = std::make_tuple(13, 3);
	spriteData["combo"] = std::make_tuple(14, 6);
	spriteData["hit"] = std::make_tuple(15, 1);
	spriteData["facehit"] = std::make_tuple(16, 1);
	spriteData["crouchhit"] = std::make_tuple(17, 0);
	spriteData["knockdown"] = std::make_tuple(18, 5);
	spriteData["ko"] = std::make_tuple(19, 3);
	spriteData["timeOver"] = std::make_tuple(20, 0);
	spriteData["victory"] = std::make_tuple(21, 2);

}




 