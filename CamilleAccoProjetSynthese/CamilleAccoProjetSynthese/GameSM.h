#ifndef GAMESM_H
#define GAMESM_H

#include "StateMachine.h"

class GameSM : public StateMachine
{
public:
	GameSM();
	~GameSM() = default;

private:
	// virtual function from StateMachine
	void initializeSM() override;
	void initializeState() override;
	void initializeStateTransition() override;

	/**************************  STATES  *****************************/

	State* mainMenu = new State("mainMenu");
	State* quit = new State("quit");
	State* options = new State("options");
	State* highScore = new State("highScore");
	State* nameSelect = new State("nameSelect");
	State* characterSelect = new State("characterSelect");
	State* stageSelect = new State("stageSelect");
	State* inGame = new State("inGame");
	State* pause = new State("pause");
	State* gameOver = new State("gameOver");

	/**************************  TRANSITIONS  *****************************/
	
	// Main menu transitions
	Transition* qMainMenu = new Transition(StateManager::w_quit); // quit game
	Transition* oMainMenu = new Transition(StateManager::w_options); // go to options menu
	Transition* hMainMenu = new Transition(StateManager::w_highscore); // go to highscore list
	Transition* nMainMenu = new Transition(StateManager::w_select); // go to name selection

	// Option transition
	Transition* bOptions = new Transition(StateManager::w_back); // go back to main menu

	// High score transition
	Transition* bHighScore = new Transition(StateManager::w_back); // go back to main menu

	// Name selection transition
	Transition* bNameSelect = new Transition(StateManager::w_back); // back to main menu
	Transition* cNameSelect = new Transition(StateManager::w_select); // go to character selection

	// Character selection transition
	Transition* bCharacterSelect = new Transition(StateManager::w_back); // back name selection
	Transition* sCharacterSelect = new Transition(StateManager::w_select); // go to stage selection

	// Stage selection transition
	Transition* bStageSelect = new Transition(StateManager::w_back); // back character selection
	Transition* sStageSelect = new Transition(StateManager::w_select); // start game

	// Pause transition
	Transition* bPause = new Transition(StateManager::w_back); // go back to game
	Transition* qPause= new Transition(StateManager::w_quit); // go back to main menu
	Transition* oPause = new Transition(StateManager::w_options); // go to options

	// Game transition
	Transition* pGame = new Transition(StateManager::w_pause); // go to pause screen
	Transition* dGame = new Transition(StateManager::w_dead); // game is over

	//Transition GameOver
	Transition * qGameOver = new Transition(StateManager::w_quit); // go back to main menu
	Transition * rGameOver = new Transition(StateManager::w_select); // restart match

};


#endif // !GAMESM_H
