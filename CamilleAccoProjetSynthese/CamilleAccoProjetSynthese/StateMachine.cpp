#include "StateMachine.h"



void StateMachine::process(StateManager event) {
	Transition* transition = current.isTransitioning(event);

	std::string oldState = " ";
	std::string newState = " ";
	
	if (transition != nullptr) {
		oldState += current.getStateName();
		current = *transition->getNextState();
		newState += current.getStateName();
	}

}

void StateMachine::addState(State* state) {
	states.push_back(state);
}

std::vector<State*> StateMachine::getStates() {
	return states;
}

void StateMachine::initialize() {
	current = initial;
}
