#ifndef ARENA_H
#define ARENA_H
#include <vector>

#include "Surface.h"
#include "Player.h"


class Arena :public Surface 
{
public:
	Arena() = default;
	Arena(size_t const &width, size_t const &height, sf::Font const &font, std::vector<Player*>& listPlayer, sf::RenderWindow& window);
	~Arena() = default;

	void fill() override;
	void update();
	void draw() override;
	/*Transitionning sprite sheet*/
	void isIdle(Player* player);
	void isWalk(Player* player);
	void isJump(Player* player);
	void isJumpPunch(Player* player);
	void isJumpKick(Player* player);
	void isBlock(Player* player);
	void isCrouch(Player* player);
	void isCrouchPunch(Player* player);
	void isCrouchKick(Player* player);
	void isCrouchHit(Player* player);
	void isLightPunch(Player* player);
	void isHardPunch(Player* player);
	void isLightKick(Player* player);
	void isHardKick(Player* player);
	void isCombo(Player* player);
	void isHadoken(Player* player);
	void isHit(Player* player);
	void isFaceHit(Player* player);
	void isKnockDown(Player* player);
	void isKO(Player* player);
	void isTimeOver(Player* player);
	void isVictory(Player* player);


private:
	sf::RenderWindow window;
	size_t maxJumpHeight{ 150}, jumpHeight{ 0 };

};

#endif // !ARENA_H

