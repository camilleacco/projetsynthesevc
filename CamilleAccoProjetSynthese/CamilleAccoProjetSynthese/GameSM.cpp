#include "GameSM.h"



GameSM::GameSM()
{
	this->initializeSM();
	
}

void GameSM::initializeSM()
{
	this->initializeState();
	this->initializeStateTransition();

	this->initial = *this->mainMenu;
	this->current = *this->mainMenu;
}

void GameSM::initializeState()
{
	this->addState(mainMenu);
	this->addState(options);
	this->addState(highScore);
	this->addState(nameSelect);
	this->addState(characterSelect);
	this->addState(stageSelect);
	this->addState(inGame);
	this->addState(pause);
	this->addState(gameOver);
	this->addState(quit);
}

void GameSM::initializeStateTransition()
{{
			// STARTING FROM MAIN MENU
			// TO QUIT
			mainMenu->addTransition(qMainMenu);
			qMainMenu->setNextState(quit);

			// TO OPTIONS
			mainMenu->addTransition(oMainMenu);
			oMainMenu->setNextState(options);

			// TO HIGHSCORE
			mainMenu->addTransition(hMainMenu);
			hMainMenu->setNextState(highScore);

			// TO NAME SELECT
			mainMenu->addTransition(nMainMenu);
			nMainMenu->setNextState(nameSelect);

			// STARTING FROM OPTIONS
			// TO MAIN
			options->addTransition(bOptions);
			bOptions->setNextState(mainMenu);


			// STARTING FROM HIGH SCORE
			// TO MAIN
			highScore->addTransition(bHighScore);
			bHighScore->setNextState(mainMenu);


			// STARTING FROM NAME SELECT
			// TO MAIN
			nameSelect->addTransition(bNameSelect);
			bNameSelect->setNextState(mainMenu);

			//TO CHARACTER SELECT
			nameSelect->addTransition(cNameSelect);
			cNameSelect->setNextState(characterSelect);

			// STARTING FROM CHARACTER SELECT
			// TO NAME SELECT
			characterSelect->addTransition(bCharacterSelect);
			bCharacterSelect->setNextState(nameSelect);

			// TO STAGE SELECT
			characterSelect->addTransition(cNameSelect);
			cNameSelect->setNextState(stageSelect);

			// STARTING FROM STAGE SELECT
			// TO CHARACTER SELECT
			stageSelect->addTransition(bStageSelect);
			bStageSelect->setNextState(nameSelect);

			// TO INGAME
			stageSelect->addTransition(sStageSelect);
			sStageSelect->setNextState(inGame);


			// STARTING FROM PAUSE
			// TO INGAME
			pause->addTransition(bPause);
			bPause->setNextState(inGame);

			// TO QUIT
			pause->addTransition(qPause);
			qPause->setNextState(quit);

			// TO OPTIONS
			pause->addTransition(oPause);
			oPause->setNextState(options);

			//STARTING FROM INGAME
			//TO PAUSE
			inGame->addTransition(pGame);
			pGame->setNextState(pause);

			//TO GAMEOVER
			inGame->addTransition(dGame);
			dGame->setNextState(gameOver);

			//STARTING FROM GAMEOVER
			// TO MAIN MENU
			gameOver->addTransition(qGameOver);
			qGameOver->setNextState(mainMenu);

			//TO RESTART MATCH
			gameOver->addTransition(rGameOver);
			rGameOver->setNextState(inGame);

		}
	}

