#ifndef VIEW_H
#define VIEW_H

#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include "Model.h"
#include "GameSM.h"
#include "State.h"
#include "MainMenu.h"


// Singleton since we only need one instance of the window
class View
{

public:
	static View& getInstance();
	void open(size_t width, size_t height, std::string title, Model& model, GameSM & gsm);
	void gameLoop();
	void findEvent(sf::Event event);
	//void drawState(std::string stateName);
	

private:
	View();
	~View();
	size_t width, height;
	sf::RenderWindow mWindow;
	sf::Font font;
	Surface* mainmenu;
	std::vector <Player*> playerList;
	GameSM gamesm;
	State lastState;
	void drawMainMenu();
	void mainmenuAction(int & key);
	void drawStates();
	void drawOptions();
	void drawHighScore();
	void drawNameSelect();
	void drawCharacterSelect();
	void drawStageSelect();
	void drawInGame();
	void drawPause();
	void drawGameover();

	


};

#endif // !VIEW_H
